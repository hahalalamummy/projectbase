//
//  UIStackView+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 31/12/2021.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

extension UIStackView {
    
    func removeAllArrangedSubviews() {
        arrangedSubviews.forEach { (view) in
            removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
    
}
