//
//  UINavigationController+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 8/20/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

extension UINavigationController {
    
    func setHiddenNaviBar() {
        self.isNavigationBarHidden = true
        self.navigationBar.isTranslucent = false
//        self.edgesForExtendedLayout = []
    }
    
    func popViewController(animated: Bool, completion: (() -> Void)? = nil) {
        // https://github.com/cotkjaer/UserInterface/blob/master/UserInterface/UINavigationViewController.swift
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        popViewController(animated: true)
        CATransaction.commit()
    }
}
