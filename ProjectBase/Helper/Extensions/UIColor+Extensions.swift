//
//  UIColor+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

extension UIColor {
    
    convenience init(hexString: String) {
        let scanner = Scanner(string: hexString)
        scanner.charactersToBeSkipped = CharacterSet.alphanumerics.inverted
        
        var value: UInt64 = 0
        scanner.scanHexInt64(&value)
        
        let red = CGFloat(Float(Int(value >> 16) & 0x000000FF) / 255.0)
        let green = CGFloat(Float(Int(value >> 8) & 0x000000FF) / 255.0)
        let blue = CGFloat(Float(Int(value) & 0x000000FF) / 255.0)

        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    // swiftlint:disable large_tuple
    convenience init(rgba: (r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat)) {
        self.init(red: rgba.r/255, green: rgba.g/255, blue: rgba.b/255, alpha: rgba.a)
    }
    
    // swiftlint:disable identifier_name
    func toHexString() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb: Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format: "#%06x", rgb)
    }
}
