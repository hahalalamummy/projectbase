//
//  DropDown+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 20/12/2021.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation
import DropDown

extension DropDown {
    
    func setupDropdown() {
        DispatchQueue.main.async {
            self.setupCornerRadius(10)
        }
        self.backgroundColor = .white
        self.shadowOffset = CGSize(width: 1, height: 1)
        self.cellHeight = 30
        self.shadowColor = UIColor(red: 0.447, green: 0.486, blue: 0.565, alpha: 0.8)
        self.textFont = FontFamily.Inter.thin.font(size: 13)
        self.textColor = Colors.darkGrey.color
    }
}
