//
//  UICollectionView+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

extension UICollectionView {
    
    func registerCell<T: UICollectionViewCell>(_ cells: T.Type...) {
        for cell in cells {
            self.register(UINib(nibName: cell.className, bundle: nil), forCellWithReuseIdentifier: cell.className)
        }
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(cell: T.Type, forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: cell.className, for: indexPath) as? T else {
            fatalError(" Could not dequeue cell with identifier: \(cell.className)")
        }
        return cell
    }
    
    func scrollToTop(animated: Bool = true) {
        setContentOffset(CGPoint.zero, animated: animated)
    }
    
    func reloadData(_ completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion: { _ in
            completion()
        })
    }
    
}
