//
//  String+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

extension String {
    // Common
    
    func trimmingSpace() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPhone() -> Bool {
        let type: NSTextCheckingResult.CheckingType = .phoneNumber
        
        if let detector = try? NSDataDetector.init(types: type.rawValue) {
            let matchs = detector.matches(in: self, options: .anchored, range: NSRange(location: 0, length: self.count))
            if matchs.count == 1 {
                return true
            }
        }
        return false
    }
    
    func evaluate(regex: String) -> Bool {
        let test = NSPredicate(format: "SELF MATCHES %@", regex)
        return test.evaluate(with: self)
    }
    
    func isValidUserName() -> Bool {
        let userNameRegEx = "[A-Za-z0-9._]{8,30}$"
        return evaluate(regex: userNameRegEx)
    }
    
    func isValidFullNameCharacter() -> Bool {
        let fullNameRegEx = "[\\d\\p{L} ]+$"
        return evaluate(regex: fullNameRegEx)
    }
    
    func isValidCardNumber() -> Bool {
        let cardRegEx = "^[0-9]+$"
        return evaluate(regex: cardRegEx)
    }
    
    func isValidPassword() -> Bool {
        let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d@$!%*#?&]{6,}$"
        return evaluate(regex: passwordRegEx)
    }
    
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
}

extension String {
    
    func caculateHeight(maxWidth: CGFloat = .greatestFiniteMagnitude, numberOfLines: Int = 0, font: UIFont) -> CGFloat {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: maxWidth, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = numberOfLines
        label.font = font
        label.lineBreakMode = .byWordWrapping
        label.text = self
        label.sizeToFit()
        return label.frame.height
    }
    
}

extension String {
    
    func toDate(format: String, timeZone: TimeZoneType = .utc) -> Date {
        return Date(fromString: self, format: .custom(format), timeZone: timeZone) ?? Date()
    }
    
    func toDouble() -> Double {
        return (self as NSString).doubleValue
    }
    
    func toInt() -> Int {
        return (self as NSString).integerValue
    }
}

extension String {
    
    func htmlAttributedString(font: UIFont) -> NSMutableAttributedString {
        
        let modifiedFont = String(format:
                                    "<span style=\"font-family: '\(font.familyName)', '\(font.fontName)'; font-size: \(font.pointSize)\">%@</span>",
                                  self)
        
        guard let data = modifiedFont.data(using: String.Encoding.utf8, allowLossyConversion: false)
        else { return NSMutableAttributedString() }
        
        guard let formattedString = try? NSMutableAttributedString(data: data,
                                                                   options: [.documentType: NSAttributedString.DocumentType.html,
                                                                             .characterEncoding: String.Encoding.utf8.rawValue],
                                                                   documentAttributes: nil )
        
        else { return NSMutableAttributedString() }
        
        return formattedString
    }
    
}
