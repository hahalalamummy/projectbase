//
//  Date+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

extension Date {
    
    var month: String {
        self.toString(format: .custom("MM"), locale: Locale(identifier: "en_US_POSIX"))
    }
    
    var dayOfWeek: String {
        self.toString(format: .custom("EEE"), locale: Locale(identifier: "en_US_POSIX"))
    }
    
    var hour: String {
        self.toString(format: .custom("HH"), locale: Locale(identifier: "en_US_POSIX"))
    }
    
    var minute: String {
        self.toString(format: .custom("mm"), locale: Locale(identifier: "en_US_POSIX"))
    }
    
    func isSameDate(as date: Date) -> Bool {
        self.compare(.isSameDay(as: date))
    }
    
}

extension Date {
    
    func tomorrowDate() -> Date {
        add(day: 1)
    }
    
    func add(year: Int) -> Date {
        add(component: .year, value: year)
    }
    
    func add(month: Int) -> Date {
        add(component: .month, value: month)
    }
    
    func add(week: Int) -> Date {
        add(component: .weekdayOrdinal, value: week)
    }
    
    func add(day: Int) -> Date {
        add(component: .day, value: day)
    }
    
    func add(minute: Int) -> Date {
        add(component: .minute, value: minute)
    }
    
    func add(second: Int) -> Date {
        add(component: .second, value: second)
    }
    
    private func add(component: Calendar.Component, value: Int) -> Date {
        let calendar = Calendar.current
        let newDate = calendar.date(byAdding: component, value: value, to: self)
        guard let date = newDate else {
            fatalError()
        }
        return date
    }
    
}
