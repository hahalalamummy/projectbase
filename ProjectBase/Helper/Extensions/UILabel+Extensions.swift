//
//  UILabel+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 14/12/2021.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

extension UILabel {
    
    func underline(_ underlineText: String) {
        let underlineRange = ((self.text ?? "") as NSString).range(of: underlineText)
        let attributedText = NSMutableAttributedString(attributedString: self.attributedText ?? NSAttributedString())
        attributedText.addAttribute(.underlineStyle,
                                    value: NSUnderlineStyle.single.rawValue,
                                    range: underlineRange)
        self.attributedText = attributedText
    }
    
}
