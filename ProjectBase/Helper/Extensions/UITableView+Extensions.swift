//
//  UITableView+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

extension UITableView {
    
    func registerCell<T: UITableViewCell>(_ cell: T.Type) {
        registerCell([T.self])
    }
    
    func registerCell<T: UITableViewCell>(_ cells: [T.Type]) {
        for cell in cells {
            self.register(UINib(nibName: cell.className, bundle: nil), forCellReuseIdentifier: cell.className)
        }
    }
    
    func dequeueReusableCell<T: UITableViewCell>(cell: T.Type, forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.className, for: indexPath) as? T else {
            fatalError(" Could not dequeue cell with identifier: \(T.className)")
        }
        return cell
    }
    
    func scrollToTop(animated: Bool = true) {
        setContentOffset(CGPoint.zero, animated: animated)
    }
    
    func reloadData(_ completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion: { _ in
            completion()
        })
    }
}
