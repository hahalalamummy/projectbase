//
//  UIViewController+Extensions.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation
import Toast_Swift

extension UIViewController {
    
    func showToast(message: String, colorToast: UIColor = Colors.red.color, position: ToastPosition = .center, completion: ((_ didTap: Bool) -> Void)? = nil) {
        self.view.hideAllToasts()
        var style = ToastStyle()
        style.messageColor = .white
        style.backgroundColor = colorToast
        self.view.makeToast(message, duration: 3.0, position: position, style: style, completion: completion)
    }
    
    func embedInNavigationController() -> UINavigationController {
        let navi = UINavigationController.init(rootViewController: self)
        navi.setHiddenNaviBar()
        return navi
    }

}
