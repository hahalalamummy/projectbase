//
//  Session.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

class Session: NSObject {
    
    // Init
    private override init() {}
    static let shared = Session()
    
    private(set) var accessToken: String?
    
    var currentLanguage: Language {
        get {
            let languageCode = UserDefaults.standard.string(forKey: Constants.UserDefault.currentLanguage) ?? "en"
            return Language(rawValue: languageCode) ?? .english
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: Constants.UserDefault.currentLanguage)
        }
    }
    
    func logout() {
        self.clearUserSession()
    }
    
    func saveAccessToken(token: String) {
        accessToken = "Bearer " + token
    }
    
    func clearUserSession() {
        self.accessToken = nil
    }
}
