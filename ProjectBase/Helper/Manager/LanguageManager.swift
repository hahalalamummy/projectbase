//
//  LanguageManager.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import UIKit

class LanguageManager {
    
    static let shared = LanguageManager()
    
    let languageChanged = PublishSubject<Void>()
    
    var currentLanguage = Session.shared.currentLanguage {
        didSet {
            Session.shared.currentLanguage = currentLanguage
            languageChanged.onNext(())
        }
    }
    
    func lookupFunction(_ key: String, _ table: String, _ value: String) -> String {
        guard let bundlePath = Bundle.main.path(forResource: currentLanguage.rawValue, ofType: "lproj"), let bundle = Bundle(path: bundlePath) else {
            fatalError()
        }
        return NSLocalizedString(key, tableName: nil, bundle: bundle, value: value, comment: "")
        
    }
    
}

enum Language: String, CaseIterable {
    
    case japanese = "ja"
    case vietnamese = "vi"
    case english = "en"
    
    var apiRequest: String {
        switch self {
        case .vietnamese:
            return "vi"
        case .english:
            return "en"
        case .japanese:
            return "jp"
        }
    }
    
    var name: String {
        switch self {
        case .vietnamese:
            return "Tiếng Việt"
        case .english:
            return "English"
        case .japanese:
            return "日本語"
        }
    }
    
    static func name(byCode code: String) -> String {
        switch code {
        case "vi":
            return "Tiếng Việt"
        case "en":
            return "English"
        case "jp":
            return "日本語"
        default:
            return "日本語"
        }
    }
}
