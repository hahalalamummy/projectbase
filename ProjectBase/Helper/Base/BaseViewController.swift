//
//  BaseViewController.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    init() {
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LanguageManager.shared.languageChanged.subscribe(onNext: { [weak self] in
            self?.languageChanged()
        }).disposed(by: disposeBag)
    }
    
    func languageChanged() {
        
    }
    
    deinit {
        debugPrint("deinit \(self.className)")
    }
}
