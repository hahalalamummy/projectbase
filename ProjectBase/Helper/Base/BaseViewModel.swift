//
//  BaseViewModel.swift
//  ProjectBase
//
//  Created by Linh on 8/19/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

class BaseViewModel: NSObject {
    
    let disposeBag = DisposeBag()
    
    var message = PublishSubject<String>()
    var isShowLoading = PublishSubject<Bool>()
    
}
