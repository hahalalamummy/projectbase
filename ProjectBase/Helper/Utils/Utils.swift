//
//  Utils.swift
//  ProjectBase
//
//  Created by Linh on 8/20/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import NVActivityIndicatorView

class Utils: NSObject {
    
    class func topViewController(_ base: UIViewController? = UIApplication.shared.windows.first?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.topViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let child = base?.children.last {
            return topViewController(child)
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
    
    // MARK: - Hub
    class func showLoadingIndicator(_ message: String? = nil) {
        // stop
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        
        // start
        let activityData = ActivityData(size: CGSize.init(width: 100, height: 100),
                                        message: message,
                                        messageFont: nil,
                                        type: NVActivityIndicatorType.ballPulseSync,
                                        color: .init(hexString: "#49A9EB"),
                                        padding: 0,
                                        displayTimeThreshold: nil,
                                        minimumDisplayTime: nil,
                                        backgroundColor: nil,
                                        textColor: nil)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    class func hideLoadingIndicator() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    @discardableResult static func openURL(_ urlString: String) -> Bool {
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
            return true
        }
        return false
    }
}
