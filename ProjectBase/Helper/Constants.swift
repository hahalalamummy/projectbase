//
//  Constants.swift
//  TestGJ
//
//  Created by Linh on 10/01/2022.
//  Copyright © 2022 Linh. All rights reserved.
//

import Foundation

struct Constants {
    
    // MARK: - Application
    struct App {
        // MARK: Base

        static let baseAPIURL = config.backendUrl
        
        // MARK: DateTime
        static let inputDateFormat = "yyyy/MM/dd"
    }
    
    struct Validation {
        static let phoneMaxLength = 15
        static let maxImageSize = 5000000 // 5MB
    }
    
    struct UserDefault {
        static let currentLanguage = "currentLanguage"
    }
    
}
