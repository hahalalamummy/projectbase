//
//  AuthResponse.swift
//  ProjectBase
//
//  Created by Linh on 8/20/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

class LoginResponse: ApiResponse {
    
    var data = LoginModel()
    
    override func parsingData(json: JSON) {
        super.parsingData(json: json)
        data = LoginModel(json: json)
    }
}

struct LoginModel {
    
    var token = ""
    var refreshToken = ""
    
    init() {
        
    }
    
    init(json: JSON) {
        if let token = json["token"].string {
            self.token = token
        } else {
            token = json["access_token"].stringValue
        }
        
        refreshToken = json["refresh_token"].stringValue
    }
    
}
