//
//  AuthApi.swift
//  ProjectBase
//
//  Created by Linh on 8/20/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation
import Moya

enum AuthApi {
    case login(input: LoginInput)
}

extension AuthApi: TargetType {
    var baseURL: URL {
        return URL(string: Constants.App.baseAPIURL + "/api")!
    }
    
    var path: String {
        switch self {
        case .login:
            return "/login"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login:
            return .post
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .login(input: let input):
            return .requestParameters(parameters: input.bodyInput, encoding: JSONEncoding.default)
        }
    }
    
    var headers: [String: String]? {
        switch self {
            
        default:
            return [
                "Content-Type": "application/json",
                "X-Requested-With": "XMLHttpRequest"
            ]
        }
    }
}
