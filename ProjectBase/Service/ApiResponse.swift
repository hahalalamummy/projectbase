//
//  ApiResponse.swift
//  ProjectBase
//
//  Created by Linh on 8/20/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation
import SwiftyJSON
import Moya

class ApiResponse {
    
    let error: NSError?
    let jsonData: JSON?
    let statusCode: Int?
    let status: Int?
    var message: String?
    
    var isSuccess: Bool {
        return status == 200
    }
    
    required init(response: Response) {
        // Success case
        statusCode = response.statusCode
        let json = JSON(response.data)
        status = json["code"].int
        message = json["message"].string
        if response.isSuccess {
            if status == 200 {
                 self.error = nil
            } else {
                let code = status
                self.error = NSError(
                    domain: ServiceHelper.errorServiceDomain,
                    code: code ?? 0,
                    userInfo: [NSLocalizedDescriptionKey: message ?? Localize.Common.Message.msg998UnknowError])
            }
            let jsonData = json["data"]
            if jsonData.exists() {
                self.jsonData = jsonData
                self.parsingData(json: jsonData)
            } else {
                self.jsonData = json
            }
            return
        }
        
        // Error cases
        if response.isUnauthorized {
            self.error = NSError(
                domain: ServiceHelper.errorServiceDomain,
                code: ServiceHelper.errorCodeUnauthorized,
                userInfo: [NSLocalizedDescriptionKey: Localize.Common.Message.msg997SectionTimeout])
            self.jsonData = nil
            Session.shared.logout()
            return
        } else if response.isGatewayTimeOut {
            self.error = NSError(
                domain: ServiceHelper.errorServiceDomain,
                code: ServiceHelper.errorCodeGatewayTimeOut,
                userInfo: [NSLocalizedDescriptionKey: Localize.Common.Message.msg999ConnectionError])
            self.jsonData = nil
            return
        }
        
        let messageJson = JSON(response.data)["message"]
        if messageJson.exists() {
            self.error = NSError(
                domain: ServiceHelper.errorServiceDomain,
                code: response.statusCode,
                userInfo: [NSLocalizedDescriptionKey: messageJson.stringValue])
            self.jsonData = nil
            return
        } else {
            self.error = NSError(
                domain: ServiceHelper.errorServiceDomain,
                code: response.statusCode,
                userInfo: [NSLocalizedDescriptionKey: Localize.Common.Message.msg998UnknowError])
            self.jsonData = nil
        }
    }
    
    required init(error: Error) {
        let nsError = error as NSError
        
        self.error = NSError(
            domain: nsError.domain,
            code: nsError.code,
            userInfo: [NSLocalizedDescriptionKey: nsError.localizedDescription])
        self.jsonData = nil
        self.status = nil
        self.statusCode = nsError.code
        self.message = nsError.localizedDescription
    }
    
    // Override to parsing data
    func parsingData(json: JSON) {
        
    }
    
}

struct Pagination {
    
    var currentPage = -1
    var totalItem = 0
    var totalPage = 0
    
    init() {
        
    }
    
    init(json: JSON) {
        let jsonObject = json["pagination"]
        currentPage = jsonObject["currentPage"].intValue
        totalItem = jsonObject["totalItem"].intValue
        totalPage = jsonObject["totalPage"].intValue
    }
    
}
