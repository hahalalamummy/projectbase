//
//  AuthRequest.swift
//  ProjectBase
//
//  Created by Linh on 8/20/21.
//  Copyright © 2021 Linh. All rights reserved.
//

import Foundation

class LoginInput: ApiInput {
    
    var email = ""
    var password = ""
    
    enum ApiField: String {
        // case nickname
        case email
        case password
        // case language
    }
    
    override var bodyInput: [String: Any] {
        return [
            ApiField.email.rawValue: email,
            ApiField.password.rawValue: password
        ]
    }
}
