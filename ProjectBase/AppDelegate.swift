//
//  AppDelegate.swift
//  ProjectBase
//
//  Created by vmo on 10/01/2022.
//

import UIKit
import netfox
import Toast_Swift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        if #available(iOS 13.0, *) {
            
        } else {
            self.window = UIWindow()
            self.window?.rootViewController = SceneDelegate.getRootVC()
            self.window?.makeKeyAndVisible()
        }
        setupForLaunching()
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}

private extension AppDelegate {
    
    private func setupForLaunching() {
        
        setupToastStyle()
        setupSwizzle()
//        setupPushNotification()
        setupNetfox()
        setupUserDefault()
//        setupTimer()
    }
    
    private func setupToastStyle() {
        ToastManager.shared.duration = 5
        ToastManager.shared.isQueueEnabled = false
        ToastManager.shared.isTapToDismissEnabled = true
        ToastManager.shared.position = .center
    }
    
    private func setupSwizzle() {
        UITableViewCell.doBadSwizzleStuff()
        UITableViewHeaderFooterView.doBadSwizzleStuff()
        UICollectionViewCell.doBadSwizzleStuff()
        UICollectionReusableView.doBadSwizzleStuff()
    }
    
//    private func setupPushNotification() {
//        UNUserNotificationCenter.current().delegate = self
//        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { granted, _ in
//            DispatchQueue.main.async {
//                if granted {
//                    UIApplication.shared.registerForRemoteNotifications()
//                } else {
//                    // TODO: stuff if unsuccessful...
//                }
//            }
//        })
//    }
    
    private func setupNetfox() {
        #if DEBUG
        NFX.sharedInstance().start()
        #endif
    }
    
    private func setupUserDefault() {
        UserDefaults.standard.register(defaults: [
            Constants.UserDefault.currentLanguage: Locale.current.languageCode ?? ""
        ])
    }
    
//    private func setupTimer() {
//        if #available(iOS 13.0, *) {
//            NotificationCenter.default.addObserver(self,
//                                                   selector: #selector(willEnterBackground),
//                                                   name: UIScene.willDeactivateNotification,
//                                                   object: nil)
//            NotificationCenter.default.addObserver(self,
//                                                   selector: #selector(didEnterForeground),
//                                                   name: UIScene.didActivateNotification,
//                                                   object: nil)
//        } else {
//            NotificationCenter.default.addObserver(self,
//                                                   selector: #selector(willEnterBackground),
//                                                   name: UIApplication.willResignActiveNotification,
//                                                   object: nil)
//            NotificationCenter.default.addObserver(self,
//                                                   selector: #selector(didEnterForeground),
//                                                   name: UIApplication.didBecomeActiveNotification,
//                                                   object: nil)
//        }
//    }
//
//    @objc private func willEnterBackground(_ notification: Notification) {
//        timeEnterBackground = Date()
//    }
//
//    @objc private func didEnterForeground(_ notification: Notification) {
//        print("didEnterForeground")
//        let newDate = Date().timeIntervalSince1970
//        let oldDate = timeEnterBackground.timeIntervalSince1970
//
//        let different = Int(newDate - oldDate)
//        if different >= Constants.App.timeout && Session.shared.accessToken != nil {
//            UserViewModel().expireNotiToken(token: Session.shared.notiToken)
//            Session.shared.logout()
//        }
//    }
}
