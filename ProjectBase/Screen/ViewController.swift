//
//  ViewController.swift
//  ProjectBase
//
//  Created by Linh on 12/01/2022.
//  Copyright © 2022 Linh. All rights reserved.
//

import UIKit

final class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func languageChanged() {
        super.languageChanged()
    }

}
