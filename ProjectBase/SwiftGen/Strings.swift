// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum Localize {

  internal enum Common {
    internal enum Message {
      /// You do not have authorized to perform this action?
      internal static var msg995NotAuthorized: String { return Localize.tr("Localizable", "common.message.MSG_995_not_authorized") }
      /// Are you sure to log out?
      internal static var msg996ConfirmLogout: String { return Localize.tr("Localizable", "common.message.MSG_996_confirm_logout") }
      /// Your user session has been lost, please log in again.
      internal static var msg997SectionTimeout: String { return Localize.tr("Localizable", "common.message.MSG_997_section_timeout") }
      /// An error has occurred, please try again.
      internal static var msg998UnknowError: String { return Localize.tr("Localizable", "common.message.MSG_998_unknow_error") }
      /// Connection error!
      /// Check your internet connection and try again.
      internal static var msg999ConnectionError: String { return Localize.tr("Localizable", "common.message.MSG_999_connection_error") }
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension Localize {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = LanguageManager.shared.lookupFunction(key, table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}
