Tạo project mới

1. tạo = xcode
2. set version ios trong Deployment Info và Deployment Target
3. có support ipad, xoay màn hình ko?
4. thêm Staging trong Configurations
5. tạo IDETemplateMacros.plist vào <ProjectName>.xcodeproj/xcshareddata/IDETemplateMacros.plist
6. chạy pod init, set version ios trong pod, thêm pod, chạy pod install
7. Thay đổi folder structure:<br>
	&emsp;SwiftGen: thêm 3 file Strings, Assets, Fonts, Colors<br>
	&emsp;Generated: thêm Config/Config.plist<br>
	&emsp;Screen<br>
	&emsp;Service: Copy project cũ vào<br>
	&emsp;Helper: Copy project cũ vào<br>
	&emsp;Resources:<br>
		&emsp;&emsp;Assets: kéo Assets.xcassets vào, thêm Colors.xcassets, xóa color trong Assets.xcassets<br>
		&emsp;&emsp;Fonts: kéo font vào<br>
	&emsp;Configuration:<br>
		&emsp;&emsp;Development, Staging, Production: copy project cũ vào, ko target vào project<br>
		&emsp;&emsp;Tạo file object-c, auto add Bridge Header rồi xóa<br>
		&emsp;&emsp;Thêm file ConfigLoader (copy project cũ vào)<br>
		&emsp;&emsp;kéo Info.plist vào (sửa cả đường dẫn trong Build Settings)<br>
	&emsp;Localize, AppDelegate, SceneDelegate, LaunchScreen<br>
8. xóa Main storyboard, xóa cả trong Main Interface, cả trong Info.plist, chọn LaunchScreen
9. thêm Localizations: tạo file string tên Localizable, giữ nguyên Group, thư mục chọn Base .lproj
10. add language mới
11. add .swiftlint.yml và swiftgen.yml, có lookupFunction hay ko tùy vào app tự set language hay theo language máy
12. Thêm Build Phases Copy config, Run SwiftGen, Run AssetChecker, run Swiftlint, kéo lại thứ tự
13. edit scheme thành Dev, Staging, Production, select lại Configurations
14. sửa lại header
15. có support dark mode ko
16. copy code setup vào AppDelegate, SceneDelegate
17. add git ignore
